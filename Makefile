up: docker-up
down: docker-down
restart: docker-down docker-up
init: docker-down-clear docker-pull docker-build docker-up list-init

docker-up:
	docker-compose up -d

docker-down:
	docker-compose down --remove-orphans

docker-down-clear:
	docker-compose down -v --remove-orphans

docker-pull:
	docker-compose pull

docker-build:
	docker-compose build

cli:
	docker-compose run --rm list-php-cli php bin/app.php

list-init: list-composer-install list-migrations

list-migrations:
	docker-compose run --rm list-php-cli php bin/console doctrine:migrations:migrate --no-interaction
	
list-composer-install:
	docker-compose run --rm list-php-cli composer install
