<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PostRepository")
 */
class Post
{
    public const STATUS_ACQUIRED = 'acquired';
    public const STATUS_NOT_ACQUIRED = 'not_acquired';

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @var \DateTimeImmutable
     * @ORM\Column(type="datetime_immutable")
     */
    private $date;

    /**
     * @var integer
     * @ORM\Column(type="integer")
     */
    private $price;

    /**
     * @var string
     * @ORM\Column(type="string", length=16)
     */
    private $status;

    public function __construct()
    {
        $this->date = new \DateTimeImmutable();
        $this->status = self::STATUS_NOT_ACQUIRED;
    }

    public function acquirate(): void
    {
        if ($this->isAcquired()) {
            throw new \DomainException('Post is already acquired.');
        }
        $this->status = self::STATUS_ACQUIRED;
    }

    public function isAcquired(): bool
    {
        return $this->status === self::STATUS_ACQUIRED;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getDate(): \DateTimeImmutable
    {
        return $this->date;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getPrice(): ?int
    {
        return $this->price;
    }

    public function setPrice(int $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getStatus(): string
    {
        return $this->status;
    }
}
