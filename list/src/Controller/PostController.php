<?php

namespace App\Controller;

use App\Entity\Post;
use App\Form\PostType;
use App\Repository\PostRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Nelmio\ApiDocBundle\Annotation\Security;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;

/**
 * @Route("/api", name="api_")
 */
class PostController extends FOSRestController
{
    /**
     * *@Rest\Get("/posts")
     *
     * @return Response
     */
    public function index(PostRepository $postRepository): Response
    {
        $posts = $postRepository->findAll();

        return $this->handleView($this->view($posts));
    }

    /**
     * *@Rest\Post("/posts")
     *
     * @return Response
     *
     * @SWG\Response(
     *     response=200,
     *     description="Возвращяет новое желание",
     *     @Model(type=Post::class)
     * )
     * @SWG\Parameter(
     *     name="form",
     *     in="body",
     *     description="Добавление нового желания",
     *     @SWG\Schema(
     *         type="object",
     *         @SWG\Property(property="title", type="string", example="Радиоуправляемый вертолет"),
     *         @SWG\Property(property="price", type="integer", example="1000")
     *     )
     * )
     */
    public function new(Request $request): Response
    {
        $post = new Post();
        $form = $this->createForm(PostType::class, $post);
        $data = json_decode($request->getContent(), true);
        $form->submit($data);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($post);
            $entityManager->flush();

            return $this->handleView($this->view(['status'=>'ok'], Response::HTTP_CREATED));
        }

        return $this->handleView($this->view($form->getErrors()));
    }

    /**
	 *
     * *@Rest\Get("/posts/{id}")
     *
     * @return Response
     */
    public function show(Post $post): Response
    {
        return $this->handleView($this->view($post));
    }

    /**
     * *@Rest\Put("/posts/{id}")
     *
     * @return Response
     *
     * @SWG\Response(
     *     response=200,
     *     description="Редактирует желание",
     *     @Model(type=Post::class)
     * )
     * @SWG\Parameter(
     *     name="form",
     *     in="body",
     *     description="Редактирование желания",
     *     @SWG\Schema(
     *         type="object",
     *         @SWG\Property(property="title", type="string", example="Световой меч"),
     *         @SWG\Property(property="price", type="integer", example="15000")
     *     )
     * )
     */
    public function edit(Request $request, Post $post): Response
    {
        $form = $this->createForm(PostType::class, $post);
        $data = json_decode($request->getContent(), true);
        $form->submit($data, false);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->handleView($this->view(['status'=>'ok'], Response::HTTP_CREATED));
        }

        return $this->handleView($this->view($form->getErrors()));
    }

    /**
     * *@Rest\Delete("/posts/{id}")
     *
     * @return Response
     */
    public function delete(Request $request, Post $post): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($post);
        $entityManager->flush();

        return $this->handleView($this->view(['status'=>'ok'], Response::HTTP_CREATED));
    }

    /**
     * *@Rest\Put("/posts/{id}/mark")
     *
     * @return Response
     */
    public function mark(Request $request, Post $post): Response
    {
        $post->acquirate();
        $this->getDoctrine()->getManager()->flush();

        return $this->handleView($this->view(['status'=>'ok'], Response::HTTP_CREATED));
    }
}
