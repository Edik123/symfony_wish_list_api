<?php

namespace App\Form\DataTransformer;

use Symfony\Component\Form\DataTransformerInterface;

class CentToDollarTransformer implements DataTransformerInterface
{
    /**
     * Transforms cent to dollar amount.
     *
     * @param int $priceInCent
     * @return double
     */
    public function transform($priceInCent)
    {
        $priceInDollar = number_format(($priceInCent / 100), 2, '.', ' ');

        return $priceInDollar;
    }

    /**
     * Transforms dollar to cent amount.
     *
     * @param double $priceInDollar
     * @return int
     */
    public function reverseTransform($priceInDollar)
    {
        $priceInCent = (int)($priceInDollar * 100);

        return $priceInCent;
    }
}
