<?php

namespace App\Form;

use App\Entity\Post;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use App\Form\DataTransformer\CentToDollarTransformer;

class PostType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', Type\TextType::class, ['label' => 'Title'])
            ->add(
                'price',
                MoneyType::class,
                array(
                    'scale' => 2,
                    'currency' => null,
                    'label' => 'Price',
                    'attr' => array(
                        'min' => '0.00',
                        'max' => '1000.00',
                        'step' => '0.01'
                    )
                )
            )
            ->add('save', SubmitType::class);

        $builder
            ->get('price')
            ->addModelTransformer(new CentToDollarTransformer());
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => Post::class,
                'csrf_protection'=>false
            ]
        );
    }
}
